# **Metapod App**

### Initial setup
-----------------
1. npm install -g webpack
2. npm install

### Build
---------
- npm run-script build

### Run Webpack
--------------------------
- npm start

## Webstorm(IntelliJ) support
1. ESLint support
	- Settings (Ctrl + Alt + S)
	- Languages & Frameworks **>** JavaScript **>** Code Quality Tools **>** ESLint
	- Checked: *Enable*
	- In most cases this would work, but you can always provide path to .eslintrc explicitly

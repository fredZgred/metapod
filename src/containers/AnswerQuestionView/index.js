import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';
import { fetchQuestionDataById, answerQuestion } from 'actions/QuestionsActions';

import * as styles from './styles';
import { CircularProgress, Paper, Divider, FloatingActionButton, List, ListItem }
    from 'material-ui';
import CreateAndUpdateInfo from 'components/CreateAndUpdateInfo';
import AnswerDialog from 'components/AnswerDialog';
import ContentAdd from 'material-ui/svg-icons/content/add';
import ContentBack from 'material-ui/svg-icons/content/undo';

const propTypes = {
    dispatch: PropTypes.func.isRequired,
    params: PropTypes.object.isRequired,
    isFetchingQuestion: PropTypes.bool.isRequired,
    isFetchingAnswers: PropTypes.bool.isRequired,
    question: PropTypes.object.isRequired,
    answers: PropTypes.array,
    questionError: PropTypes.string,
    answersError: PropTypes.string,
};

class AnswerQuestionView extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isAnswering: false,
        };

        this.handleGoBack = this.handleGoBack.bind(this);
        this.handleAnswerClick = this.handleAnswerClick.bind(this);
        this.handleAnswerCancel = this.handleAnswerCancel.bind(this);
        this.handleAnswerSubmit = this.handleAnswerSubmit.bind(this);
    }

    componentWillMount() {
        const { dispatch, params } = this.props;

        dispatch(fetchQuestionDataById(params.id));
    }

    handleGoBack() {
        browserHistory.goBack();
    }

    handleAnswerClick() {
        this.setState({ isAnswering: true });
    }

    handleAnswerCancel() {
        this.setState({ isAnswering: false });
    }

    handleAnswerSubmit(answer) {
        const { dispatch, params } = this.props;

        dispatch(answerQuestion(params.id, answer));
        this.setState({ isAnswering: false });
    }

    renderQuestion() {
        if (!this.props.question) return null;

        const { title, body, createDate, updateDate } = this.props.question;
        const { questionError: error } = this.props;
        let renderQuestion = (
            <article>
                <header><h1>{title}</h1></header>
                <p>{body}</p>
                <Divider />
                <footer>
                    {createDate &&
                        <CreateAndUpdateInfo
                            prefix="Asked"
                            createDate={createDate}
                            updateDate={updateDate}
                        />
                    }
                </footer>
            </article>
        );

        if (error) {
            renderQuestion = <p>There was a problem fetching question.</p>;
        }

        return renderQuestion;
    }

    renderAnswers() {
        const { answers } = this.props;
        let renderAnswers = ([
            <ListItem
                key="no_answers"
                primaryText="This question doesn't have answers yet. Please answer if you can."
            />,
            <Divider key="no_answers_divider" />,
        ]);

        if (answers.length) {
            renderAnswers = answers.map(answer => ([
                <ListItem
                    key={answer.id}
                    primaryText="Check answer"
                    secondaryText={
                        <CreateAndUpdateInfo
                            prefix="Added"
                            createDate={answer.createDate}
                            updateDate={answer.updateDate}
                        />
                    }
                    primaryTogglesNestedList
                    nestedItems={[
                        <ListItem key="answer">{answer.body}</ListItem>,
                    ]}
                />,
                <Divider key="answers_divider" />,
            ]));
        }

        return renderAnswers;
    }

    render() {
        const { isFetchingQuestion, isFetchingAnswers } = this.props;

        return (
            <div className="container">
                <AnswerDialog
                    isOpened={this.state.isAnswering}
                    onSubmit={this.handleAnswerSubmit}
                    onCancel={this.handleAnswerCancel}
                />
                <div style={styles.qBtnsRow}>
                    <FloatingActionButton
                        onTouchTap={this.handleGoBack}
                    >
                        <ContentBack />
                    </FloatingActionButton>
                    <FloatingActionButton
                        onTouchTap={this.handleAnswerClick}
                    >
                        <ContentAdd />
                    </FloatingActionButton>
                </div>
                <Paper
                    zDepth={2}
                    style={styles.question}
                >
                    {isFetchingQuestion
                        ? <CircularProgress size={2} />
                        : this.renderQuestion()
                    }
                </Paper>
                <List>
                    {isFetchingAnswers
                        ? <CircularProgress size={1} />
                        : this.renderAnswers()
                    }
                </List>
            </div>
        );
    }
}

AnswerQuestionView.propTypes = propTypes;

export default connect(
    state => ({
        isFetchingQuestion: state.answerQuestion.isFetchingQuestion,
        isFetchingAnswers: state.answerQuestion.isFetchingAnswers,
        question: state.answerQuestion.question,
        answers: state.answerQuestion.answers,
        questionError: state.questions.questionError,
        answersError: state.questions.answersError,
    })
)(AnswerQuestionView);

export const question = {
    padding: '2rem',
};

export const qBtnsRow = {
    display: 'flex',
    justifyContent: 'space-between',
    width: '100%',
    margin: '2rem 0',
};

import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Paper, TextField, RaisedButton, FlatButton, Dialog } from 'material-ui';
import { Link } from 'react-router';
import { lightGreen900 } from 'material-ui/styles/colors';

import * as styles from './styles';
import { signUp, signUpPopupShown } from 'actions/UserActions';

const propTypes = {
    dispatch: PropTypes.func.isRequired,
    signedUp: PropTypes.bool.isRequired,
    location: PropTypes.object.isRequired,
};

class SignUpView extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isModalOpen: false,
        };

        this.submit = this.submit.bind(this);
        this.toggleModal = this.toggleModal.bind(this);
        this.setEmailIfReceived = this.setEmailIfReceived.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        const { dispatch, signedUp } = nextProps;

        if (signedUp) {
            this.toggleModal();
            dispatch(signUpPopupShown());
        }
    }

    toggleModal() {
        this.setState({
            isModalOpen: !this.state.isModalOpen,
        });
    }

    setEmailIfReceived() {
        const { location } = this.props;

        return location.query.email;
    }

    submit(e) {
        e.preventDefault();

        const { dispatch, location } = this.props;
        const { email, nickName, password, passwordConfirmation } = e.target.elements;

        const userData = {
            email: email.value,
            nickName: nickName.value,
            password: password.value,
            passwordConfirmation: passwordConfirmation.value,
        };

        dispatch(signUp(userData, location.query.token));
    }

    render() {
        const modalButtons = [
            <FlatButton
                onClick={this.toggleModal}
                label="Ok"
                primary
            />,
        ];

        return (
            <div style={styles.container}>
                <Paper
                    zDepth={2}
                    className="paper"
                >
                    <h2>Create new account</h2>
                    <form
                        onSubmit={this.submit}
                    >
                        <TextField
                            style={styles.input}
                            name="email"
                            value={this.setEmailIfReceived()}
                            floatingLabelText="Email address"
                        />
                        <TextField
                            style={styles.input}
                            name="nickName"
                            floatingLabelText="Nick name"
                        />
                        <TextField
                            style={styles.input}
                            name="password"
                            floatingLabelText="Password"
                        />
                        <TextField
                            style={styles.input}
                            name="passwordConfirmation"
                            floatingLabelText="Password again"
                        />
                        <RaisedButton
                            type="submit"
                            label="Register"
                            primary
                        />
                        <span className="space"></span>
                        or
                        <span className="space"></span>
                        <Link to="/sign-in">
                            <FlatButton
                                style={{ border: `1px solid ${lightGreen900}` }}
                                label="Sign in"
                                primary
                            />
                        </Link>
                    </form>
                </Paper>
                <Dialog
                    title="Success"
                    actions={modalButtons}
                    open={this.state.isModalOpen}
                >
                    New account created successfully. Now you have to confirm
                    your email address. Check your inbox.
                </Dialog>
            </div>
        );
    }
}

SignUpView.propTypes = propTypes;

export default connect(
    state => ({
        signedUp: state.user.signedUp,
        location: state.routing.locationBeforeTransitions,
    })
)(SignUpView);

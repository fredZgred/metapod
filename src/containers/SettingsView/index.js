import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { Paper, TextField, RaisedButton } from 'material-ui';
import { Table, TableBody, TableRow, TableRowColumn } from 'material-ui/Table';

import Avatar from 'components/Avatar/';
import LoadingSpinner from 'components/LoadingSpinner/';
import { getMembers, inviteUser, connectDatabase } from 'actions/InstanceActions';

const propTypes = {
    dispatch: PropTypes.func.isRequired,
    instance: PropTypes.object.isRequired,
    members: PropTypes.array.isRequired,
    membersReady: PropTypes.bool.isRequired,
};

class SettingsView extends Component {

    componentWillMount() {
        const { dispatch } = this.props;

        dispatch(getMembers());

        this.inviteUser = this.inviteUser.bind(this);
        this.connectDatabase = this.connectDatabase.bind(this);
    }

    inviteUser(e) {
        const { dispatch } = this.props;

        e.preventDefault();

        const inputs = e.target.elements;
        dispatch(inviteUser(inputs.email.value));
    }

    connectDatabase(e) {
        const { dispatch } = this.props;

        e.preventDefault();

        const inputs = e.target.elements;
        dispatch(connectDatabase(inputs.database.value));
    }

    render() {
        const { instance, members, membersReady } = this.props;

        const tableOfMembers = membersReady ? (
            <Table>
                <TableBody
                    showRowHover
                    stripedRows
                    displayRowCheckbox={false}
                >
                    {members.map(
                        (member, index) => (
                            <TableRow key={index}>
                                <TableRowColumn>
                                    <Avatar
                                        style={{margin: '10px'}}
                                        emailHash={member.emailHash}
                                    />
                                </TableRowColumn>
                                <TableRowColumn className="member-name">
                                    {member.nickName}
                                </TableRowColumn>
                            </TableRow>
                        )
                    )}
                </TableBody>
            </Table>
        ) : (
            <LoadingSpinner />
        );

        return (
            <div className="container settings-view clearfix">
                <div className="row">
                    <div className="col tile tile-members">
                        <Paper className="paper">
                            <h1>Members of {instance.name} metapod</h1>
                            {tableOfMembers}
                        </Paper>
                    </div>
                    <div className="col tile tile-others">
                        <Paper className="paper">
                            <h1>Invite people</h1>
                            <form onSubmit={this.inviteUser}>
                                <TextField
                                    className="input-wide"
                                    type="email"
                                    name="email"
                                    hintText="Provide email of person you want to invite"
                                />
                                <div className="text-right">
                                    <RaisedButton
                                        type="submit"
                                        label="Invite"
                                        primary
                                    />
                                </div>
                            </form>
                            <h1>Connect own database</h1>
                            <form onSubmit={this.connectDatabase}>
                                <TextField
                                    className="input-wide"
                                    type="text"
                                    name="database"
                                    value={instance.databaseUrl}
                                    hintText="Your own database url"
                                />
                                <div className="text-right">
                                    <RaisedButton
                                        type="submit"
                                        label="connect"
                                        primary
                                    />
                                </div>
                            </form>
                        </Paper>
                    </div>
                </div>
            </div>
        );
    }
}

SettingsView.propTypes = propTypes;

export default connect(
    state => ({
        instance: state.instance.body,
        members: state.instance.members,
        membersReady: state.instance.membersReady,
    })
)(SettingsView);

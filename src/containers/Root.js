import React, { PropTypes } from 'react';
import { Provider } from 'react-redux';
import { Router } from 'react-router';

// material-ui dependency
// http:// www.material-ui.com/#/get-started/installation
import injectTapEventPlugin from 'react-tap-event-plugin';

import routes from 'routes';

const propTypes = {
    store: PropTypes.object.isRequired,
    history: PropTypes.object.isRequired,
};

export default function Root({ store, history }) {
    injectTapEventPlugin();
    return (
        <Provider store={store}>
            <Router
                history={history}
                routes={routes}
            />
        </Provider>
    );
}

Root.propTypes = propTypes;

import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { AppBar, IconButton, FontIcon } from 'material-ui';
import AppBarUserStatus from 'components/AppBarUserStatus';
import SideMenu from 'components/SideMenu';
import { signOut } from 'actions/UserActions';

const propTypes = {
    dispatch: PropTypes.func.isRequired,
    user: PropTypes.object,
    instance: PropTypes.object,
    location: PropTypes.object.isRequired,
};

class NavBar extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isSideMenuOpen: false,
        };

        this.signOut = this.signOut.bind(this);
        this.toggleSideMenu = this.toggleSideMenu.bind(this);
    }

    signOut() {
        const { dispatch } = this.props;

        dispatch(signOut());
    }

    toggleSideMenu() {
        this.setState({ isSideMenuOpen: !this.state.isSideMenuOpen });
    }

    render() {
        const { user, instance, location } = this.props;
        const selectedMetapodRoutes = ['/', '/settings'];

        let appTitle = 'Metapod';
        if (instance && selectedMetapodRoutes.indexOf(location.pathname) > -1) {
            appTitle += ` - ${instance.name}`;
        }

        const drawerToggleButton = (
            <IconButton
                iconStyle={{ color: '#fff' }}
                onClick={this.toggleSideMenu}
            >
                <FontIcon
                    className="fa fa-bars"
                />
            </IconButton>
        );

        return (
            <AppBar
                title={appTitle}
                iconElementLeft={drawerToggleButton}
            >
                <AppBarUserStatus
                    user={user}
                    handleSignOut={this.signOut}
                />
                <SideMenu
                    user={user}
                    instance={instance}
                    isOpen={this.state.isSideMenuOpen}
                    handleSignOut={this.signOut}
                    handleToggleSideMenu={this.toggleSideMenu}
                />
            </AppBar>
        );
    }
}

NavBar.propTypes = propTypes;

export default connect(
    state => ({
        user: state.user.body,
        instance: state.instance.body,
        location: state.routing.locationBeforeTransitions,
    })
)(NavBar);

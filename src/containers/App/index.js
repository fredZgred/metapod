import React, { Component, PropTypes } from 'react';
import { metapodMuiTheme } from 'material-ui-themes/metapod';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import LoadingSpinner from 'components/LoadingSpinner/';

import { connect } from 'react-redux';
import { getCurrentUser, getCurrentUserSuccess } from 'actions/UserActions';
import { getCurrentInstance, getCurrentInstanceSuccess } from 'actions/InstanceActions';

import NavBar from '../NavBar';

const propTypes = {
    children: PropTypes.node.isRequired,
    dispatch: PropTypes.func.isRequired,
    user: PropTypes.object,
    userReady: PropTypes.bool.isRequired,
    instanceReady: PropTypes.bool.isRequired,
};

class App extends Component {

    componentWillMount() {
        const { dispatch } = this.props;

        if (localStorage.token) {
            dispatch(getCurrentUser());
            dispatch(getCurrentInstance());
        } else {
            dispatch(getCurrentUserSuccess());
            dispatch(getCurrentInstanceSuccess());
        }
    }

    render() {
        const { children, user, userReady, instanceReady } = this.props;
        const navBar = user ? <NavBar /> : null;
        const appLoaded = userReady && instanceReady;
        const content = appLoaded ? children : <LoadingSpinner />;

        return (
            <MuiThemeProvider muiTheme={metapodMuiTheme}>
                <div>
                    {navBar}
                    {content}
                </div>
            </MuiThemeProvider>
        );
    }
}

App.propTypes = propTypes;

export default connect(
    state => ({
        user: state.user.body,
        userReady: state.user.userReady,
        instanceReady: state.instance.instanceReady,
    })
)(App);

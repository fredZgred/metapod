import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { push as redirect } from 'react-router-redux';
import { Paper } from 'material-ui';

import CreateInstanceModal from './createInstanceModal';
import {
    getMyInstances, getMyInvitations, enterInstance, createInstance,
    acceptInvitation, refreshAfterAcceptInvitation,
} from 'actions/InstanceActions';

const propTypes = {
    dispatch: PropTypes.func.isRequired,
    instance: PropTypes.object,
    instancesList: PropTypes.array.isRequired,
    invitations: PropTypes.array.isRequired,
    invitationAccepted: PropTypes.bool.isRequired,
};

class DashboardView extends Component {

    constructor(props) {
        super(props);

        this.state = {
            isCreateInstanceModalOpen: false,
        };

        this.handleEnterMetapod = this.handleEnterMetapod.bind(this);
        this.createInstance = this.createInstance.bind(this);
        this.toggleCreateInstanceModal =
            this.toggleCreateInstanceModal.bind(this);
        this.acceptInvitation = this.acceptInvitation.bind(this);
    }

    componentWillMount() {
        const { dispatch } = this.props;

        dispatch(getMyInstances());
        dispatch(getMyInvitations());
    }

    componentWillReceiveProps(nextProps) {
        const { dispatch, invitationAccepted } = nextProps;

        if (invitationAccepted) {
            dispatch(refreshAfterAcceptInvitation());
        }
    }

    handleEnterMetapod(instanceId) {
        const { dispatch, instance } = this.props;

        if (instance && instanceId === instance.id) {
            dispatch(redirect('/'));
        } else {
            dispatch(enterInstance(instanceId));
        }
    }

    createInstance(name) {
        const { dispatch } = this.props;

        dispatch(createInstance(name));
    }

    toggleCreateInstanceModal() {
        this.setState({
            isCreateInstanceModalOpen: !this.state.isCreateInstanceModalOpen,
        });
    }

    acceptInvitation(instanceId) {
        const { dispatch } = this.props;

        dispatch(acceptInvitation(instanceId));
    }

    render() {
        const { instancesList, invitations } = this.props;

        const instancesNarrower = invitations.length ? 'invitations-narrower' : '';

        return (
            <div className="container dashboard-view">
                <div className="row">
                    <div className={`col instances ${instancesNarrower}`}>
                        <Paper className="paper">
                            <h1>Metapods that you belongs to</h1>
                            {!!instancesList.length && instancesList.map(
                                instance => (
                                    <button
                                        key={instance.id}
                                        onClick={() => this.handleEnterMetapod(instance.id)}
                                    >
                                        Enter {instance.name} metapod
                                    </button>
                                )
                            )}
                            <br />
                            <br />
                            <button
                                onClick={this.toggleCreateInstanceModal}
                            >
                                Create new instance
                            </button>
                        </Paper>
                    </div>
                    {!!invitations.length && (
                        <div className="col invitations">
                            <Paper className="paper">
                                <h1>Invitations</h1>
                                {invitations.map(
                                    invitation => (
                                        <div
                                            className="clearfix"
                                            style={{ borderBottom: '1px solid #000' }}
                                            key={invitation.id}
                                        >
                                            {invitation.name}
                                            <button
                                                style={{ float: 'right' }}
                                                onClick={() => this.acceptInvitation(invitation.id)}
                                            >
                                                accept
                                            </button>
                                        </div>
                                    )
                                )}
                            </Paper>
                        </div>
                    )}
                </div>
                <CreateInstanceModal
                    handleCreateInstance={this.createInstance}
                    handleToggleCreateInstanceModal={
                        this.toggleCreateInstanceModal
                    }
                    isOpen={this.state.isCreateInstanceModalOpen}
                />
            </div>
        );
    }
}

DashboardView.propTypes = propTypes;

export default connect(
    state => ({
        instance: state.instance.body,
        instancesList: state.instance.list,
        invitations: state.instance.invitations,
        invitationAccepted: state.instance.invitationAccepted,
    })
)(DashboardView);

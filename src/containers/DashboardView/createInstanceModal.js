import React, { Component, PropTypes } from 'react';
import { Dialog, FlatButton, TextField } from 'material-ui';

const propTypes = {
    handleCreateInstance: PropTypes.func.isRequired,
    handleToggleCreateInstanceModal: PropTypes.func.isRequired,
    isOpen: PropTypes.bool.isRequired,
};

export default class CreateInstanceModal extends Component {

    constructor(props) {
        super(props);

        this.submit = this.submit.bind(this);
    }

    submit(e) {
        const { handleCreateInstance } = this.props;

        e.preventDefault();

        const inputs = e.target.elements;
        handleCreateInstance(inputs.name.value);
    }

    render() {
        const { handleToggleCreateInstanceModal, isOpen } = this.props;

        return (
            <Dialog
                title="Create New Metapod"
                modal
                open={isOpen}
            >
                <form onSubmit={this.submit} >
                    <TextField
                        name="name"
                        floatingLabelText="Your New Metapod Name"
                        floatingLabelFixed
                    />
                    <br />
                    <FlatButton
                        label="Cancel"
                        onTouchTap={handleToggleCreateInstanceModal}
                    />
                    <FlatButton
                        type="submit"
                        label="Create"
                        primary
                    />
                </form>
            </Dialog>
        );
    }
}

CreateInstanceModal.propTypes = propTypes;

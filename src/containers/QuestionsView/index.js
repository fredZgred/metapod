import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { fetchQuestions, askQuestion, openQuestion } from 'actions/QuestionsActions';

import { CircularProgress } from 'material-ui';
import QuestionsTabs from 'components/QuestionsTabs';

const propTypes = {
    dispatch: PropTypes.func.isRequired,
    items: PropTypes.array.isRequired,
    isFetching: PropTypes.bool.isRequired,
    error: PropTypes.string,
};

class QuestionsView extends Component {

    constructor(props) {
        super(props);

        this.handleOpenQuestion = this.handleOpenQuestion.bind(this);
        this.handleAskQuestion = this.handleAskQuestion.bind(this);
    }

    componentWillMount() {
        this.props.dispatch(fetchQuestions());
    }

    handleOpenQuestion(questionId) {
        this.props.dispatch(openQuestion(questionId));
    }

    handleAskQuestion(title, body) {
        this.props.dispatch(askQuestion(title, body));
    }

    render() {
        const { isFetching, items, error } = this.props;

        return (
            <section className="container">
                {isFetching
                    ? <CircularProgress size={1} />
                    : <QuestionsTabs
                        error={error}
                        questions={items}
                        handleOpenQuestion={this.handleOpenQuestion}
                        handleAskQuestion={this.handleAskQuestion}
                    />
                }
            </section>
        );
    }
}

QuestionsView.propTypes = propTypes;

export default connect(
    state => ({
        items: state.questions.items,
        isFetching: state.questions.isFetching,
        error: state.questions.error,
    })
)(QuestionsView);

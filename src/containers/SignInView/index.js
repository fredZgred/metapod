import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { signIn } from 'actions/UserActions';
import { push as redirect } from 'react-router-redux';

import * as styles from './styles';
import SignInForm from 'components/SignInForm';
import Paper from 'material-ui/Paper';

const propTypes = {
    dispatch: PropTypes.func,
    signingIn: PropTypes.bool,
    failedSignIn: PropTypes.bool,
    user: PropTypes.object,
    location: PropTypes.object.isRequired,
};

class SignInView extends Component {

    constructor(props) {
        super(props);

        this.handleSignIn = this.handleSignIn.bind(this);
    }

    componentWillMount() {
        const { dispatch, user } = this.props;

        if (user) {
            dispatch(redirect('/dashboard'));
        }
    }

    handleSignIn(login, password) {
        const { signingIn, dispatch, location } = this.props;

        if (signingIn) return;

        dispatch(signIn(login, password, location.query.token));
    }

    render() {
        let signInError = null;

        if (this.props.failedSignIn) {
            signInError = 'Please correct your email/login or password.';
        }

        return (
            <div style={styles.container}>
                <Paper
                    zDepth={2}
                    style={styles.paper}
                >
                    <h2>Welcome to Metapod</h2>
                    <SignInForm
                        handleSignIn={this.handleSignIn}
                        signInError={signInError}
                    />
                </Paper>
            </div>
        );
    }
}

SignInView.propTypes = propTypes;

export default connect(
    state => ({
        signingIn: state.user.signingIn,
        failedSignIn: state.user.failedSignIn,
        user: state.user.body,
        location: state.routing.locationBeforeTransitions,
    })
)(SignInView);

export const container = {
    height: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
};

export const paper = {
    padding: '4rem',
};

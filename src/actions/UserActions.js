import * as types from 'constants/ActionTypes';
import request from 'utils/requester';
import { push as redirect } from 'react-router-redux';

import { clearInstance } from './InstanceActions';

function signInRequest() {
    return {
        type: types.SIGN_IN_REQUEST,
    };
}

function signUpSuccess() {
    return {
        type: types.SIGN_UP_SUCCESS,
    };
}

export function signUpPopupShown() {
    return {
        type: types.SIGN_UP_POPUP_SHOWN,
    };
}

function signInSuccess() {
    return {
        type: types.SIGN_IN_SUCCESS,
    };
}

function signInFailed(error) {
    return {
        type: types.SIGN_IN_FAILED,
        error,
    };
}

function doSignOut() {
    return {
        type: types.SIGN_OUT,
    };
}

function getCurrentUserRequest() {
    return {
        type: types.GET_CURRENT_USER_REQUEST,
    };
}

export function getCurrentUserSuccess(user = null) {
    return {
        type: types.GET_CURRENT_USER_SUCCESS,
        body: user,
    };
}

export function getCurrentUser() {
    return dispatch => {
        dispatch(getCurrentUserRequest());

        request('/api/users/me/optional', {
            method: 'GET',
        })
            .then(user => {
                dispatch(getCurrentUserSuccess(user.id && user || null));
            });
    };
}

function authorizationTokenReceived(token) {
    return dispatch => {
        localStorage.token = token;

        // TODO: make sign in request returning user body as well as token
        dispatch(getCurrentUser());
        dispatch(redirect('/dashboard'));
    };
}

export function signUp(signUpData, token) {
    return dispatch => {
        request('/api/users/register', {
            method: 'POST',
            body: {
                signUpData,
                token,
            },
        })
            .then(response => {
                if (response.token) {
                    authorizationTokenReceived(response.token)(dispatch);
                } else {
                    dispatch(signUpSuccess());
                }
            });
    };
}

export function signIn(emailOrNickname, password, token) {
    return dispatch => {
        dispatch(signInRequest());

        request('/api/users/auth', {
            method: 'POST',
            body: {
                emailOrNickName: emailOrNickname,
                password,
                token,
            },
        })
            .then(response => {
                dispatch(signInSuccess());
                authorizationTokenReceived(response.token)(dispatch);
            })
            .catch(error => dispatch(signInFailed(error)));
    };
}

export function signOut() {
    localStorage.removeItem('token');

    return dispatch => {
        dispatch(redirect('/sign-in'));
        dispatch(doSignOut());
        dispatch(clearInstance());
    };
}

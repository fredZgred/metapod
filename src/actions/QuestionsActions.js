import * as types from 'constants/ActionTypes';
import request from 'utils/requester';
import { push as redirect } from 'react-router-redux';

function fetchQuestionsRequest() {
    return {
        type: types.FETCH_QUESTIONS_REQUEST,
    };
}

function fetchQuestionsSuccess(questions) {
    return {
        type: types.FETCH_QUESTIONS_SUCCESS,
        questions,
    };
}

function fetchQuestionsFailed(error) {
    return {
        type: types.FETCH_QUESTIONS_FAILED,
        error,
    };
}

export function fetchQuestions() {
    return dispatch => {
        dispatch(fetchQuestionsRequest());

        request('/api/questions', {
            method: 'GET',
        })
            .then(questions => dispatch(fetchQuestionsSuccess(questions)))
            .catch(error => dispatch(fetchQuestionsFailed(error.message)));
    };
}

function askQuestionSuccess(question) {
    return {
        type: types.ASK_QUESTION_SUCCESS,
        question,
    };
}

function askQuestionFailed(error) {
    return {
        type: types.ASK_QUESTION_FAILED,
        error,
    };
}

export function askQuestion(title, body) {
    return dispatch => {
        const question = {
            title,
            body,
        };

        request('/api/questions', {
            method: 'POST',
            body: {
                question,
            },
        })
            .then(resultQuestion => dispatch(askQuestionSuccess(resultQuestion)))
            .catch(error => dispatch(askQuestionFailed(error.message)));
    };
}

export function openQuestion(questionId) {
    return redirect(`/questions/${questionId}`);
}

export function fetchAnswersRequest() {
    return {
        type: types.FETCH_QUESTION_ANSWERS_REQUEST,
    };
}

export function fetchAnswersSuccess(answers) {
    return {
        type: types.FETCH_QUESTION_ANSWERS_SUCCESS,
        answers,
    };
}

export function fetchAnswersFailed(error) {
    return {
        type: types.FETCH_QUESTION_ANSWERS_FAILED,
        error,
    };
}

export function fetchAnswers(questionId) {
    return dispatch => {
        dispatch(fetchAnswersRequest());

        request(`/api/answers/${questionId}`, {
            method: 'GET',
        })
            .then(answers => dispatch(fetchAnswersSuccess(answers)))
            .catch(error => dispatch(fetchAnswersFailed(error)));
    };
}

function fetchQuestionDataRequest() {
    return {
        type: types.FETCH_QUESTION_DATA_REQUEST,
    };
}

function fetchQuestionDataSuccess(question) {
    return {
        type: types.FETCH_QUESTION_DATA_SUCCESS,
        question,
    };
}

function fetchQuestionDataFailed(error) {
    return {
        type: types.FETCH_QUESTION_DATA_FAILED,
        error,
    };
}

export function fetchQuestionDataById(questionId) {
    return dispatch => {
        dispatch(fetchQuestionDataRequest());

        request(`/api/questions/${questionId}`, {
            method: 'GET',
        })
            .then(question => dispatch(fetchQuestionDataSuccess(question)))
            .catch(error => dispatch(fetchQuestionDataFailed(error.message)))
            .then(dispatch(fetchAnswers(questionId)));
    };
}

function answerQuestionSuccess(answer) {
    return {
        type: types.ANSWER_QUESTION_SUCCESS,
        answer,
    };
}

function answerQuestionFailed(error) {
    return {
        type: types.ANSWER_QUESTION_FAILED,
        error,
    };
}

export function answerQuestion(questionId, body) {
    return dispatch => {
        request(`/api/answers/${questionId}`, {
            method: 'POST',
            body: {
                body,
            },
        })
            .then(resultAnswer => dispatch(answerQuestionSuccess(resultAnswer)))
            .catch(error => dispatch(answerQuestionFailed(error)));
    };
}

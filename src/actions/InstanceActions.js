import * as types from 'constants/ActionTypes';
import request from 'utils/requester';
import { push as redirect } from 'react-router-redux';

function getMyInstancesSuccess(instances) {
    return {
        type: types.GET_MY_INSTANCES_SUCCESS,
        list: instances,
    };
}

function getMyInvitationsSuccess(invitations) {
    return {
        type: types.GET_MY_INVITATIONS_SUCCESS,
        invitations,
    };
}

function enterInstanceFailed() {
    return {
        type: types.ENTER_INSTANCE_FAILED,
    };
}

function getCurrentInstanceRequest() {
    return {
        type: types.GET_CURRENT_INSTANCE_REQUEST,
    };
}

function getMembersSuccess(members = []) {
    return {
        type: types.GET_MEMBERS_SUCCESS,
        members,
    };
}

function acceptInvitationSuccess() {
    return {
        type: types.ACCEPT_INVITATION_SUCCESS,
    };
}

export function clearInstance() {
    return {
        type: types.CLEAR_INSTANCE,
    };
}

function refreshedAfterInvitationAccept() {
    return {
        type: types.REFRESHED_AFTER_INVITATION_ACCEPT,
    };
}

export function getCurrentInstanceSuccess(instance = null) {
    return {
        type: types.GET_CURRENT_INSTANCE_SUCCESS,
        body: instance,
    };
}

export function getMyInstances() {
    return dispatch => {
        request('/api/instances', {
            method: 'GET',
        })
            .then(response => {
                dispatch(getMyInstancesSuccess(response));
            });
    };
}

export function getMyInvitations() {
    return dispatch => {
        request('/api/instances/invitations', {
            method: 'GET',
        })
            .then(response => {
                dispatch(getMyInvitationsSuccess(response));
            });
    };
}

export function getCurrentInstance() {
    return dispatch => {
        dispatch(getCurrentInstanceRequest());

        request('/api/instances/current/optional', {
            method: 'GET',
        })
            .then(instance => {
                dispatch(getCurrentInstanceSuccess(
                    instance.id && instance || null
                ));
            });
    };
}

function instanceTokenReceived(dispatch) {
    return response => {
        localStorage.token = response.token;

        // TODO: make sign in request returning user body as well as token
        dispatch(getCurrentInstance());
        dispatch(redirect('/'));
    };
}

export function createInstance(instanceName) {
    return dispatch => {
        request('/api/instances', {
            method: 'POST',
            body: {
                name: instanceName,
            },
        })
            .then(instanceTokenReceived(dispatch));
    };
}

export function enterInstance(instanceId) {
    return dispatch => {
        request(`/api/instances/${instanceId}/enter`, {
            method: 'POST',
        })
            .then(instanceTokenReceived(dispatch))
            .catch(error => dispatch(enterInstanceFailed(error)));
    };
}

export function getMembers() {
    return dispatch => {
        request('/api/instances/current/members', {
            method: 'GET',
        })
            .then(members => dispatch(getMembersSuccess(members)))
            .catch(() => dispatch(getMembersSuccess()));
    };
}

export function inviteUser(email) {
    return dispatch => {
        request('/api/instances/current/invite', {
            method: 'POST',
            body: {
                email,
            },
        });
    };
}

export function acceptInvitation(instanceId) {
    return dispatch => {
        request(`/api/instances/${instanceId}/invitation/accept`, {
            method: 'POST',
        })
            .then(() => dispatch(acceptInvitationSuccess()));
    };
}

export function refreshAfterAcceptInvitation() {
    return dispatch => {
        dispatch(getMyInstances());
        dispatch(getMyInvitations());
        dispatch(refreshedAfterInvitationAccept());
    };
}

export function connectDatabase(databaseUrl) {
    return dispatch => {
        request('/api/instances/current/database/connect', {
            method: 'POST',
            body: {
                databaseUrl,
            },
        });
    };
}

import store from 'store';
import { signOut } from 'actions/UserActions';
import 'whatwg-fetch';

export default function request(url, config) {
    const userToken = localStorage.token;

    Object.assign(config, {
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            Authorization: userToken || void 0,
        },
        body: JSON.stringify(config.body),
    });

    return fetch(url, config)
        .then(response => {
            if (response.status >= 200 && response.status < 300) {
                return response.json();
            } else if (response.status === 401) {
                store.dispatch(signOut());
            }

            return response.json().then(error => Promise.reject(error));
        });
}

export function utcToCurrentTimezone(date, revert = false) {
    const d = new Date(date);

    return revert
        ? d.setMinutes(d.getMinutes() + (new Date()).getTimezoneOffset())
        : d.setMinutes(d.getMinutes() - (new Date()).getTimezoneOffset());
}

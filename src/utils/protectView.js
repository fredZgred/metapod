import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { push as redirect } from 'react-router-redux';

import LoadingSpinner from 'components/LoadingSpinner/';

const propTypes = {
    dispatch: PropTypes.func.isRequired,
    data: PropTypes.object.isRequired,
    userReady: PropTypes.bool,
    instanceReady: PropTypes.bool,
    location: PropTypes.object.isRequired,
    props: PropTypes.object,
};

export default function(ProtectedView) {
    let typeOfProtection;

    class Protect extends Component {

        componentWillMount() {
            this.checkAndRedirectIfNeeded();
        }

        componentWillReceiveProps() {
            this.checkAndRedirectIfNeeded();
        }

        checkAndRedirectIfNeeded() {
            const { dispatch, data, userReady, instanceReady, location } = this.props;

            if (!data[typeOfProtection]) {
                switch (typeOfProtection) {

                    case 'user':
                        if (location.pathname !== '/sign-in' && userReady) {
                            dispatch(redirect('/sign-in'));
                        }
                        return;

                    case 'instance':
                        if (
                            location.pathname !== '/dashboard' &&
                            instanceReady
                        ) {
                            dispatch(redirect('/dashboard'));
                        }
                        return;

                    case 'instanceOwner':
                        if (
                            location.pathname !== '/sign-in' &&
                            userReady &&
                            instanceReady
                        ) {
                            dispatch(redirect('/sign-in'));
                        }
                        return;

                    default:
                        return;
                }
            }
        }

        render() {
            const { data, props } = this.props;
            const content = data[typeOfProtection] ? (
                <ProtectedView { ...props } />
            ) : <LoadingSpinner />;

            return (
                <div>
                    {content}
                </div>
            );
        }
    }

    Protect.propTypes = propTypes;

    function run() {
        return connect(
            (state, props) => ({
                data: {
                    user: state.user.body,
                    instance: state.instance.body,
                    instanceOwner: state.user.body &&
                                   state.instance.body &&
                                   state.user.body.id ===
                                   state.instance.body.ownerId,
                },
                userReady: state.user.userReady,
                instanceReady: state.instance.instanceReady,
                location: props.location,
                props,
            })
        )(Protect);
    }

    return {
        requireUser() {
            typeOfProtection = 'user';
            return run();
        },
        requireInstance() {
            typeOfProtection = 'instance';
            return run();
        },
        requireInstanceOwner() {
            typeOfProtection = 'instanceOwner';
            return run();
        },
    };
}

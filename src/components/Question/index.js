import React, { PropTypes } from 'react';
import * as styles from './styles';
import { ListItem, Avatar } from 'material-ui';

import CreateAndUpdateInfo from 'components/CreateAndUpdateInfo';

import questionIcon from './icon_question.svg';

const propTypes = {
    title: PropTypes.string.isRequired,
    ownerId: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    createDate: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    updateDate: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    onClick: PropTypes.func.isRequired,
};

export default function Question({ onClick, title, createDate, updateDate }) {
    const info = (
        <CreateAndUpdateInfo
            prefix="Asked"
            createDate={createDate}
            updateDate={updateDate}
        />
    );

    return (
        <ListItem
            onClick={onClick}
            leftAvatar={<Avatar src={questionIcon} style={styles.questionIcon} />}
            primaryText={title}
            secondaryText={info}
            secondaryTextLines={1}
        />
    );
}

Question.propTypes = propTypes;

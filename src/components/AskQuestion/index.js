import React, { Component, PropTypes } from 'react';
import * as styles from './styles';
import { TextField, RaisedButton } from 'material-ui';

const propTypes = {
    title: PropTypes.string,
    body: PropTypes.string,
    onSubmit: PropTypes.func.isRequired,
};

export default class AskQuestion extends Component {

    constructor(props) {
        super(props);
        this.askQuestion = this.askQuestion.bind(this);
    }

    askQuestion(e) {
        e.preventDefault();

        const { title, body } = e.target.elements;

        this.props.onSubmit(title.value, body.value);
        e.target.reset();
    }

    render() {
        return (
            <section className="container">
                <form
                    onSubmit={this.askQuestion}
                    style={{ textAlign: 'center' }}
                >
                    <TextField
                        onChange={e => e.stopPropagation()}
                        type="text"
                        name="title"
                        floatingLabelText="Title"
                        floatingLabelFixed
                        style={styles.input}
                    />
                    <TextField
                        onChange={e => e.stopPropagation()}
                        type="text"
                        name="body"
                        floatingLabelText="Content"
                        floatingLabelFixed
                        style={styles.input}
                    />
                    <RaisedButton
                        type="submit"
                        label="Submit"
                        primary
                    />
                </form>
            </section>
        );
    }
}

AskQuestion.propTypes = propTypes;

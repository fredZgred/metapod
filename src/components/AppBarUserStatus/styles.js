export const appBarUserStatus = {
    display: 'flex',
    WebkitAlignItems: 'center',
    alignItems: 'center',
};

export const nickName = {
    marginLeft: '10px',
    display: 'inline-block',
    color: '#fff',
    fontSize: '20px',
};

export const signOutButton = {
    display: 'inline-block',
};

export const signOutButtonIconStyle = {
    color: '#fff',
};

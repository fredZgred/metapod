import React, { PropTypes } from 'react';
import { FontIcon, IconButton } from 'material-ui';

import * as styles from './styles';
import Avatar from 'components/Avatar/';

const propTypes = {
    handleSignOut: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired,
};

export default function AppBarUserStatus({ user, handleSignOut }) {
    return (
        <div
            style={styles.appBarUserStatus}
        >
            <Avatar emailHash={user.emailHash} />
            <div
                style={styles.nickName}
            >
                {user.nickName}
            </div>
            <IconButton
                iconStyle={styles.signOutButtonIconStyle}
                onClick={handleSignOut}
                style={styles.signOutButton}
            >
                <FontIcon
                    className="fa fa-sign-out"
                />
            </IconButton>
        </div>
    );
}

AppBarUserStatus.propTypes = propTypes;

import React, { PropTypes } from 'react';
import TimeAgo from 'react-timeago';

import { utcToCurrentTimezone } from 'utils/time';

const propTypes = {
    createDate: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    updateDate: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
    prefix: PropTypes.string.isRequired,
};

export default function CreateAndUpdateInfo({ prefix, createDate, updateDate }) {
    return (
        <p style={{ marginBottom: 0 }}>
            {prefix}: <TimeAgo date={utcToCurrentTimezone(createDate)} />
            {updateDate !== createDate &&
                <span>
                    , Updated: <TimeAgo date={utcToCurrentTimezone(updateDate)} />
                </span>
            }
        </p>
    );
}

CreateAndUpdateInfo.propTypes = propTypes;

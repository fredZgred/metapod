import React, { PropTypes } from 'react';
import { Drawer, IconButton, FontIcon, MenuItem, Divider } from 'material-ui';
import { Link } from 'react-router';

import UserStatus from './UserStatus';

import * as styles from './styles';

const propTypes = {
    handleSignOut: PropTypes.func.isRequired,
    handleToggleSideMenu: PropTypes.func.isRequired,
    isOpen: PropTypes.bool.isRequired,
    user: PropTypes.object,
    instance: PropTypes.object,
};

export default function SideMenu({
    handleSignOut,
    handleToggleSideMenu,
    isOpen,
    user,
    instance,
}) {
    let menuButtonsWhenInstanceSelected = instance && [
        <Link
            key="sideMenuButton.home"
            to="/"
            style={styles.menuButton}
        >
            <MenuItem onClick={handleToggleSideMenu}>
                <FontIcon
                    className="fa fa-home"
                />
                <span className="space"></span>
                Current metapod
            </MenuItem>
        </Link>,
        <Divider key="sideMenuDivider" />,
    ];

    if (menuButtonsWhenInstanceSelected && user.id === instance.ownerId) {
        menuButtonsWhenInstanceSelected.splice(1, 0, (
            <Link
                key="sideMenuButton.settings"
                to="/settings"
                style={styles.menuButton}
            >
                <MenuItem onClick={handleToggleSideMenu}>
                    Settings
                </MenuItem>
            </Link>
        ));
    }

    return (
        <Drawer
            open={isOpen}
            docked={false}
        >
            <IconButton
                iconStyle={styles.closeSideMenuButtonIconStyle}
                onClick={handleToggleSideMenu}
                style={styles.closeSideMenuButton}
            >
                <FontIcon
                    className="fa fa-times"
                />
            </IconButton>
            <UserStatus
                user={user}
                handleSignOut={handleSignOut}
            />
            {menuButtonsWhenInstanceSelected}
            <Link
                to="/dashboard"
                style={styles.menuButton}
            >
                <MenuItem onClick={handleToggleSideMenu}>
                    Dashboard
                </MenuItem>
            </Link>
        </Drawer>
    );
}

SideMenu.propTypes = propTypes;

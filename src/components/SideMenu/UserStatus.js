import React, { PropTypes } from 'react';
import { Paper, IconButton, FontIcon } from 'material-ui';

import * as styles from './styles';
import Avatar from 'components/Avatar/';

const propTypes = {
    handleSignOut: PropTypes.func.isRequired,
    user: PropTypes.object.isRequired,
};

export default function UserStatus({ handleSignOut, user }) {
    return (
        <Paper
            zDepth={0}
            style={styles.UserStatus}
        >
            <div>
                <Avatar
                    style={styles.avatar}
                    emailHash={user.emailHash}
                />
                <IconButton
                    iconStyle={{ color: '#fff' }}
                    onClick={handleSignOut}
                    style={styles.signOutButton}
                >
                    <FontIcon
                        className="fa fa-sign-out"
                    />
                </IconButton>
                <div className="clearfix"></div>
            </div>
            <div
                style={styles.nickName}
            >
                {user.nickName}
            </div>
        </Paper>
    );
}

UserStatus.propTypes = propTypes;

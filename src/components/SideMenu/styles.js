import {
    lightGreen900,
} from 'material-ui/styles/colors';

// SideMenu(index.js)
export const closeSideMenuButton = {
    display: 'inline-block',
    float: 'right',
};

export const closeSideMenuButtonIconStyle = {
    color: '#fff',
};

export const menuButton = {
    textDecoration: 'none',
};

// UserStatus
export const UserStatus = {
    background: lightGreen900,
    padding: '30px',
    borderRadius: 0,
};

export const avatar = {
    float: 'left',
};

export const signOutButton = {
    marginLeft: '10px',
    float: 'left',
};

export const nickName = {
    marginTop: '10px',
    display: 'inline-block',
    color: '#fff',
    fontSize: '20px',
};

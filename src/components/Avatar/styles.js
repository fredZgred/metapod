export const avatar = {
    display: 'inline-block',
    width: '48px',
    height: '48px',
    overflow: 'hidden',
    background: 'none',
    borderRadius: '50%',
};

export const gravatar = {
    height: '100%',
    width: '100%',
};
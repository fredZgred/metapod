import React, { PropTypes } from 'react';
import { Paper } from 'material-ui';
import Gravatar from 'react-gravatar';

import * as styles from './styles';

const propTypes = {
    emailHash: PropTypes.string.isRequired,
    style: PropTypes.object,
};

export default function Avatar({ emailHash, style }) {
    return (
        <Paper
            style={Object.assign({}, styles.avatar, style)}
        >
            <Gravatar
                email={emailHash}
                style={styles.gravatar}
            />
        </Paper>
    );
}

Avatar.propTypes = propTypes;

import React, { Component, PropTypes } from 'react';
import { Tabs, Tab, List, ListItem, Divider } from 'material-ui';

import Question from 'components/Question';
import AskQuestion from 'components/AskQuestion';

const propTypes = {
    error: PropTypes.string,
    questions: PropTypes.arrayOf(PropTypes.object),
    handleOpenQuestion: PropTypes.func.isRequired,
    handleAskQuestion: PropTypes.func.isRequired,
};

export default class QuestionsTabs extends Component {

    constructor(props) {
        super(props);
        this.state = {
            activeTab: 'answer',
        };

        this.handleTabChange = this.handleTabChange.bind(this);
        this.handleAskQuestion = this.handleAskQuestion.bind(this);
    }

    handleTabChange(activeTab) {
        this.setState({ activeTab });
    }

    handleAskQuestion(title, body) {
        this.props.handleAskQuestion(title, body);
        this.handleTabChange('answer');
    }

    renderQuestions() {
        const { error, questions, handleOpenQuestion } = this.props;
        let render;

        if (error) {
            render = ([
                <ListItem
                    key="errtext"
                    primaryText={error}
                    secondaryText={<p>Feel free to ask whatever you want!</p>}
                />,
                <Divider key="errdivider" />,
            ]);
        } else {
            render = questions.map(question => ([
                <Question
                    key={question.id}
                    ownerId={question.ownerId}
                    title={question.title}
                    createDate={question.createDate}
                    updateDate={question.updateDate}
                    onClick={() => handleOpenQuestion(question.id)}
                />,
                <Divider inset />,
            ]));
        }

        return render;
    }

    render() {
        return (
            <Tabs
                value={this.state.activeTab}
                onChange={this.handleTabChange}
            >
                <Tab
                    label="Answer question"
                    value="answer"
                >
                    <List>
                        {this.renderQuestions()}
                    </List>
                </Tab>
                <Tab
                    label="Ask question"
                    value="ask"
                >
                    <AskQuestion onSubmit={this.handleAskQuestion} />
                </Tab>
            </Tabs>
        );
    }
}

QuestionsTabs.propTypes = propTypes;

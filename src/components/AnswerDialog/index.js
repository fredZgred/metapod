import React, { PropTypes } from 'react';

import { Dialog, TextField, RaisedButton } from 'material-ui';

const propTypes = {
    onSubmit: PropTypes.func.isRequired,
    onCancel: PropTypes.func.isRequired,
    isOpened: PropTypes.bool.isRequired,
};

export default function AnswerDialog({ onSubmit, onCancel, isOpened }) {
    return (
        <Dialog
            modal
            open={isOpened}
            title="Please provide answer to this question"
        >
            <form
                onSubmit={e => {
                    e.preventDefault();
                    const form = e.target;
                    onSubmit(form.elements.answer.value);
                    form.reset();
                }}
            >
                <TextField
                    name="answer"
                    floatingLabelText="Your answer"
                    floatingLabelFixed
                    fullWidth
                    multiLine
                    rows={1}
                    rowsMax={4}
                />
                <RaisedButton
                    style={{ float: 'right' }}
                    label="Cancel"
                    onTouchTap={onCancel}
                />
                <RaisedButton
                    style={{ float: 'right' }}
                    label="Submit"
                    type="submit"
                />
            </form>
        </Dialog>
    );
}

AnswerDialog.propTypes = propTypes;

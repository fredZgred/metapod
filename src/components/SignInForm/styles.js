export const form = {
    textAlign: 'center',
};

export const input = {
    display: 'block',
    minWidth: '100%',
};

export const signInError = {
    minHeight: '2rem',
    margin: '2rem 0',
    lineHeight: '1.2',
    color: 'rgb(244, 67, 54)',
};

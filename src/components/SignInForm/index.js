import React, { Component, PropTypes } from 'react';
import * as styles from './styles';
import { TextField, RaisedButton, FlatButton } from 'material-ui';
import { lightGreen900 } from 'material-ui/styles/colors';
import { Link } from 'react-router';

const propTypes = {
    handleSignIn: PropTypes.func.isRequired,
    signInError: PropTypes.string,
};

export default class SignInForm extends Component {
    constructor(props) {
        super(props);

        this.state = {
            errors: {
                login: '',
                password: '',
            },
        };

        this.submit = this.submit.bind(this);
    }

    validate(login, password) {
        let isValid = true;
        const errors = {};

        if (login.length < 4) {
            isValid = false;
            errors.login = 'Minimum 4 characters required.';
        }

        if (login.length > 16) {
            isValid = false;
            errors.login = 'Maximum 16 characters available.';
        }

        if (password.length === 0) {
            isValid = false;
            errors.password = 'Password is required.';
        }

        this.setState({ errors });
        return isValid;
    }

    submit(e) {
        e.preventDefault();

        const inputs = e.target.elements;
        const login = inputs.login.value;
        const password = inputs.password.value;

        if (this.validate(login, password)) {
            this.props.handleSignIn(login, password);
        }
    }

    render() {
        return (
            <form
                onSubmit={this.submit}
                style={styles.form}
            >
                <TextField
                    name="login"
                    floatingLabelText="Login"
                    floatingLabelFixed
                    errorText={this.state.errors.login}
                    style={styles.input}
                />
                <TextField
                    type="password"
                    name="password"
                    floatingLabelText="Password"
                    floatingLabelFixed
                    errorText={this.state.errors.password}
                    style={styles.input}
                />
                <p
                    style={styles.signInError}
                >
                    {this.props.signInError}
                </p>
                <RaisedButton
                    type="submit"
                    label="Sign In"
                    primary
                />
                <span className="space"></span>
                or
                <span className="space"></span>
                <Link to="/sign-up">
                    <FlatButton
                        style={{ border: `1px solid ${lightGreen900}` }}
                        label="Register"
                        primary
                    />
                </Link>
            </form>
        );
    }
}

SignInForm.propTypes = propTypes;

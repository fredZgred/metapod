import React from 'react';
import CircularProgress from 'material-ui/CircularProgress';

import * as styles from './styles';

export default function LoadingSpinner() {
    return (
        <CircularProgress
            size={2}
            style={styles.spinner}
        />
    );
}

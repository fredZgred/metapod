import getMuiTheme from 'material-ui/styles/getMuiTheme';
import {
    lightGreen900, lightGreen700, lime200, lime600, lime900,
    lightGreenA400, white, darkBlack, fullBlack,
} from 'material-ui/styles/colors';
import { fade } from 'material-ui/utils/colorManipulator';

const metapodBaseTheme = {
    palette: {
        primary1Color: lightGreen900,
        primary2Color: lightGreen700,
        primary3Color: lime600,
        accent1Color: lightGreenA400,
        accent2Color: lime200,
        accent3Color: lime900,
        textColor: darkBlack,
        alternateTextColor: white,
        canvasColor: white,
        borderColor: lightGreen700,
        disabledColor: fade(darkBlack, 0.3),
        pickerHeaderColor: lightGreen900,
        clockCircleColor: fade(darkBlack, 0.07),
        shadowColor: fullBlack,
    },
};

export const metapodMuiTheme = getMuiTheme(metapodBaseTheme);

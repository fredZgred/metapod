import * as types from 'constants/ActionTypes';

const initialState = {
    body: null,
    signingIn: false,
    failedSignIn: false,
    userReady: false,
    signedUp: false,
};

export default function user(state = initialState, action) {
    switch (action.type) {

        case types.GET_CURRENT_USER_REQUEST:
            return Object.assign({}, state, {
                userReady: false,
            });

        case types.GET_CURRENT_USER_SUCCESS:
            return Object.assign({}, state, {
                body: action.body,
                userReady: true,
            });

        case types.SIGN_UP_SUCCESS:
            return Object.assign({}, state, {
                signedUp: true,
            });

        case types.SIGN_UP_POPUP_SHOWN:
            return Object.assign({}, state, {
                signedUp: false,
            });

        case types.SIGN_IN_REQUEST:
            return Object.assign({}, state, {
                signingIn: true,
                failedSignIn: false,
            });

        case types.SIGN_IN_SUCCESS:
            return Object.assign({}, state, {
                signingIn: false,
            });

        case types.SIGN_IN_FAILED:
            return Object.assign({}, state, {
                signingIn: false,
                failedSignIn: true,
            });

        case types.SIGN_OUT:
            return Object.assign({}, state, {
                body: null,
            });

        default:
            return state;
    }
}

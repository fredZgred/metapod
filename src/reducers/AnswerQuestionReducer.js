import * as types from 'constants/ActionTypes';

const initialState = {
    isFetchingQuestion: false,
    isFetchingAnswers: false,
    question: {},
    answers: [],
    questionError: null,
    answerError: null,
};

export default function (state = initialState, action) {
    switch (action.type) {

        case types.FETCH_QUESTION_DATA_REQUEST:
            return Object.assign({}, state, {
                isFetchingQuestion: true,
            });

        case types.FETCH_QUESTION_DATA_SUCCESS:
            return Object.assign({}, state, {
                isFetchingQuestion: false,
                question: action.question,
            });

        case types.FETCH_QUESTION_DATA_FAILED:
            return Object.assign({}, state, {
                isFetchingQuestion: false,
                questionError: action.error,
            });

        case types.FETCH_QUESTION_ANSWERS_REQUEST:
            return Object.assign({}, state, {
                isFetchingAnswers: true,
            });

        case types.FETCH_QUESTION_ANSWERS_SUCCESS:
            return Object.assign({}, state, {
                isFetchingAnswers: false,
                answers: action.answers,
            });

        case types.FETCH_QUESTION_ANSWERS_FAILED:
            return Object.assign({}, state, {
                isFetchingAnswers: false,
                answerError: action.error,
            });

        case types.ANSWER_QUESTION_SUCCESS:
            return Object.assign({}, state, {
                answers: [...state.answers, action.answer],
            });

        case types.ANSWER_QUESTION_FAILED:
            return Object.assign({}, state, {
                answerError: action.error,
            });

        default:
            return state;
    }
}

import { combineReducers } from 'redux';
import { routerReducer as routing } from 'react-router-redux';

import UserReducer from './UserReducer';
import InstanceReducer from './InstanceReducer';
import QuestionsReducer from './QuestionsReducer';
import AnswerQuestionReducer from './AnswerQuestionReducer';

export default combineReducers({
    user: UserReducer,
    instance: InstanceReducer,
    questions: QuestionsReducer,
    answerQuestion: AnswerQuestionReducer,
    routing,
});

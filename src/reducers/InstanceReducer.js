import * as types from 'constants/ActionTypes';

const initialState = {
    body: null,
    enteringFailed: false,
    instanceReady: false,
    list: [],
    invitations: [],
    invitationAccepted: false,
    members: [],
    membersReady: false,
};

export default function instance(state = initialState, action) {
    switch (action.type) {

        case types.GET_CURRENT_INSTANCE_REQUEST:
            return Object.assign({}, state, {
                instanceReady: false,
            });

        case types.GET_CURRENT_INSTANCE_SUCCESS:
            return Object.assign({}, state, {
                body: action.body,
                instanceReady: true,
            });

        case types.GET_MY_INSTANCES_SUCCESS:
            return Object.assign({}, state, {
                list: action.list,
            });

        case types.ENTER_INSTANCE_FAILED:
            return Object.assign({}, state, {
                enteringFailed: true,
            });

        case types.GET_MY_INVITATIONS_SUCCESS:
            return Object.assign({}, state, {
                invitations: action.invitations,
            });

        case types.ACCEPT_INVITATION_SUCCESS:
            return Object.assign({}, state, {
                invitationAccepted: true,
            });

        case types.REFRESHED_AFTER_INVITATION_ACCEPT:
            return Object.assign({}, state, {
                invitationAccepted: false,
            });

        case types.GET_MEMBERS_SUCCESS:
            return Object.assign({}, state, {
                members: action.members,
                membersReady: true,
            });

        case types.CLEAR_INSTANCE:
            return Object.assign({}, state, initialState, {
                instanceReady: true,
            });

        default:
            return state;
    }
}

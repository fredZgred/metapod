import * as types from 'constants/ActionTypes';

const initialState = {
    isFetching: false,
    items: [],
    error: null,
};

export default function questions(state = initialState, action) {
    switch (action.type) {
        case types.FETCH_QUESTIONS_REQUEST:
            return Object.assign({}, state, {
                isFetching: true,
            });

        case types.FETCH_QUESTIONS_SUCCESS:
            return Object.assign({}, state, {
                isFetching: false,
                items: action.questions,
            });

        case types.FETCH_QUESTIONS_FAILED:
            return Object.assign({}, state, {
                isFetching: false,
                error: action.error,
            });

        case types.ASK_QUESTION_SUCCESS:
            return Object.assign({}, state, {
                items: [action.question, ...state.items],
            });

        case types.ASK_QUESTION_FAILED:
            return Object.assign({}, state, {
                error: action.error,
            });

        default:
            return state;
    }
}

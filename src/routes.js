import React from 'react';
import { Route, IndexRoute } from 'react-router';

import protectView from 'utils/protectView';
import App from 'containers/App/';
import QuestionsView from 'containers/QuestionsView/';
import SettingsView from 'containers/SettingsView/';
import SignInView from 'containers/SignInView/';
import SignUpView from 'containers/SignUpView/';
import DashboardView from 'containers/DashboardView/';
import AnswerQuestionView from 'containers/AnswerQuestionView/';

export default (
    <Route
        path="/"
        component={App}
    >
        <IndexRoute
            component={protectView(QuestionsView).requireInstance()}
        />
        <Route
            path="settings"
            component={protectView(SettingsView).requireInstanceOwner()}
        />
        <Route
            path="dashboard"
            component={protectView(DashboardView).requireUser()}
        />
        <Route
            path="sign-in"
            component={SignInView}
        />
        <Route
            path="sign-up"
            component={SignUpView}
        />
        <Route
            path="questions/:id"
            component={AnswerQuestionView}
        />
    </Route>
);

import { createStore, applyMiddleware } from 'redux';
import { routerMiddleware } from 'react-router-redux';
import { browserHistory } from 'react-router';
import thunk from 'redux-thunk';

import reducers from 'reducers';

export default createStore(
    reducers,
    applyMiddleware(
        routerMiddleware(browserHistory),
        thunk
    )
);

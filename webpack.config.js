var path = require('path');
var webpack = require('webpack');
var CopyWebpackPlugin = require('copy-webpack-plugin');

var config = {
    devtool: 'eval-source-map',
    entry: __dirname + "/src/index.js",
    output: {
        path: __dirname + "/dist",
        filename: "metapod.js",
        library: 'MP'
    },
    resolve: {
        root: [
            path.resolve('./src')
        ]
    },
    module: {
        loaders: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: 'babel',
                query: {
                    presets: ['es2015', 'react']
                }
            },
            {
                test: /\.(woff2|woff|ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
                loader: 'file-loader'
            },
            {
                test: /\.scss$/,
                loader: 'style-loader!css-loader!postcss-loader!sass-loader'
            }
        ]
    },
    devServer: {
        contentBase: "./dist",
        colors: true,
        historyApiFallback: true,
        inline: true,
        proxy: {
            '/api*': {
                target: 'http://localhost:3000',
                secure: false
            }
        }
    }
};

if(process.env.NODE_ENV === 'production') {
    config.devtool = false;
    config.plugins = [
        new webpack.optimize.OccurenceOrderPlugin(),
        new webpack.optimize.UglifyJsPlugin({ comments: false }),
        new webpack.DefinePlugin({
            'process.env': { NODE_ENV: '"production"' }
        }),
        new CopyWebpackPlugin([
            { from: 'src/index.html' }
        ])
    ];
}

module.exports = config;

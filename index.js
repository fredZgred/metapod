const request = require('request');
const express = require('express');
const app = express();

const config = {
    backendUrl: process.env.BACKEND_URL || 'http://localhost:3000',
    port: process.env.PORT || 3001,
};

app.use('/', express.static(`${__dirname}/dist`));
app.use('/api', (req, res) => {
    const url = `${config.backendUrl}/api${req.url}`;
    req.pipe(request(url)).pipe(res);
});
app.get('*', (req, res) => {
    res.sendFile(`${__dirname}/dist/index.html`);
});

const listener = app.listen(config.port, () => {
    // eslint-disable-next-line no-console
    console.info('listening on port', listener.address().port);
});

process.on('uncaughtException', err => {
    // eslint-disable-next-line no-console
    console.error('Uncaught exception:', err.stack);
});
